﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Vuforia;

public class CloudImageTargetTracker : DefaultTrackableEventHandler
{
	#region PUBLIC_MEMBERS

	public VideoPlayer videoPlayer;

	#endregion // PUBLIC_MEMBERS


	#region PROTECTED_METHODS

	protected override void Start()
	{
		base.Start();
	}

	protected override void OnTrackingFound()
	{
		Debug.Log("------------------------------------ FOUND ------------------------------------");
		base.OnTrackingFound();
		videoPlayer.gameObject.SetActive(true);

		if (!videoPlayer.isPlaying)
		{
			videoPlayer.Play();
		}

		// Stop finder since we have now a result, finder will be restarted again when we lose track of the result
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

		if (objectTracker != null)
		{
			objectTracker.GetTargetFinder<ImageTargetFinder>().Stop();
		}
	}

	protected override void OnTrackingLost()
	{
		Debug.Log("------------------------------------ LOST ------------------------------------");

		if (!videoPlayer.isPlaying)
		{
			videoPlayer.Stop();
		}
		videoPlayer.gameObject.SetActive(false);
		
		base.OnTrackingLost();


		// Start finder again if we lost the current trackable
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

		if (objectTracker != null)
		{
			objectTracker.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);
			objectTracker.GetTargetFinder<ImageTargetFinder>().StartRecognition();
		}
	}

	#endregion //PROTECTED_METHODS
}

