﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using System;

public class UIVideo : MonoBehaviour
{
    public Image thumbnailImage;
    public TextMeshProUGUI nameText;

    [Header("VideoPlayer")]
    public VideoPlayer videoPlayer;
    public GameObject playButton;
    public GameObject pauseButton;

    #region Public Methods
    public void PlayVideo()
    {
        playButton.SetActive(false);
        thumbnailImage.gameObject.SetActive(false);
        videoPlayer.Play();   
    }

    public void Pausevideo()
    {
        if (videoPlayer.isPlaying)
        {
            videoPlayer.Pause();
            playButton.SetActive(true);
        }
    }

    public void SetThumbnail(string url)
    {
        StartCoroutine(GetTexture(CreateSpriteFromTexture, url));
    }

    #endregion

    #region Private Methods

    IEnumerator GetTexture(Action<Texture2D> callback, string url)
    {
        if (url != "")
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                callback(texture);
            }
        }
    }

    private void CreateSpriteFromTexture(Texture2D texture)
    {
        if (texture == null)
            return;
       
        thumbnailImage.overrideSprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    #endregion
}
