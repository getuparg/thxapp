﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class FooterPanel : MonoBehaviour
{
    public FooterButton[] buttons;
    public FooterButton initialButton;

    private Animator anim;
    private bool isInAR = false;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        GotoAPP();
    }

    public void DeselectButtons()
    {
        foreach (FooterButton b in buttons)
        {
            b.DeselectButton();
        }
    }

    public void GotoAR()
    {
        //anim.SetBool("Fadein", false);
        isInAR = true;
    }

    public void GotoAPP()
    {
        if (anim.GetBool("Fadein") && !isInAR)
            anim.SetBool("Fadein", false);
        else
        {
            anim.SetBool("Fadein", true);
            isInAR = false;
        }
    }
}
