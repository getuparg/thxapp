﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Product : MonoBehaviour
{
    
    [Header("Info")]
    public string name;
    public Sprite sprite;
    public Sprite backgroundImage;
    [TextArea(1,5)]
    public string description;
    public GameObject parentPanel;

    public TextMeshProUGUI nameDisplay;
    public Image targetImage;
    public Button seeMoreBtn;

    public ProductPanel productDescriptionPanel;


    private void Awake()
    {
        Init();
    }

    private void Init()
    {        
        nameDisplay.text = name != "" ? name : "";
        targetImage.overrideSprite = sprite != null ? sprite : null;
        seeMoreBtn.onClick.AddListener(OnClickProductSeeMore);
    }

    private void OnClickProductSeeMore()
    {
        ProductDescription prod;
        prod.title = name;
        prod.initialImage = sprite;
        prod.backgroundImage = backgroundImage;
        prod.description = description;

        productDescriptionPanel.SetPanel(prod, parentPanel);
        productDescriptionPanel.ShowPanel();
    }



}

public struct ProductDescription
{
    public string title;
    public string description;
    public Sprite initialImage;
    public Sprite backgroundImage;

}