﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanel : MonoBehaviour
{

    public void GoPanel(GameObject targetPanel)
    {
        gameObject.SetActive(false);
        targetPanel.SetActive(true);
    }

    public void ActivePanel()
    {
        FindObjectOfType<UIManager>().HidePanels();
        gameObject.SetActive(true);
    }
}
