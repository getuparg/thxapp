﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ProductPanel : MonoBehaviour
{
    public Image backgroundImage;
    public Image initialImage;
    public TextMeshProUGUI title;
    public TextMeshProUGUI description;

    public GameObject backPanel;
    public Button backButton;

    public void SetPanel(ProductDescription productDescription, GameObject previousPanel)
    {
        backgroundImage.overrideSprite = productDescription.backgroundImage;
        initialImage.overrideSprite = productDescription.initialImage;
        title.text = productDescription.title;
        description.text = productDescription.description;

        backPanel = previousPanel;
        backButton.onClick.AddListener(HidePanel);
    }

    public void ShowPanel()
    {
        gameObject.SetActive(true);
    }

    public void HidePanel()
    {
        gameObject.SetActive(false);
        backPanel.SetActive(true);
    }
}
