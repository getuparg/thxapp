﻿using UnityEngine;
using UnityEngine.UI;

public class FooterButton : MonoBehaviour
{
    public Sprite selectedSprite;
    public Sprite unselectedSprite;
    public Image targetImage;
    public UIPanel targetPanel;
    public bool isARButton;

    private FooterPanel footer;
    private UIHeader header;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClickFooterButton);
        footer = FindObjectOfType<FooterPanel>();
        header= FindObjectOfType<UIHeader>();
    }

    public void OnClickFooterButton()
    {
        if (isARButton)
        {
            footer.GotoAR();
            header.GotoAR();
            targetPanel.ActivePanel();
            //return;
        }

        footer.DeselectButtons();
        
        if(targetPanel != null)
            targetPanel.ActivePanel();
        
        if(selectedSprite != null)
            targetImage.overrideSprite = selectedSprite;
    }

    public void DeselectButton()
    {
        if(unselectedSprite != null)
            targetImage.overrideSprite = unselectedSprite;
    }

    public void SelectButton()
    {
        if (selectedSprite != null)
            targetImage.overrideSprite = selectedSprite;
    }

}
