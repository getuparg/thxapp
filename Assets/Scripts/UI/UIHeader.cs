﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHeader : MonoBehaviour
{
    public GameObject logo;
    public GameObject closeButton;
    public GameObject backButton;

    private Animator anim;
    private bool isInAR = false;
    private FooterPanel footer;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        GotoAPP();
    }

    public void GotoAR()
    {
        anim.SetBool("FadeAR", true);
        isInAR = true;
    }

    public void GotoAPP()
    {
        anim.SetBool("FadeAR", false);

        if (anim.GetBool("Fadein") && !isInAR)
            anim.SetBool("Fadein", false);
        else
        {
            anim.SetBool("Fadein", true);
            isInAR = false;
        }
    }
}
