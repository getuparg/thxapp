﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashController : MonoBehaviour
{
    public float splashTime = 3f;
    public string sceneToLoad = "APP";

    public GameObject onBoarding;
    private void Awake()
    {
        StartCoroutine(SplashCoroutine());    
    }

    IEnumerator SplashCoroutine()
    {
        yield return new WaitForSeconds(splashTime);

        onBoarding.GetComponent<Animator>().SetBool("Fadein",true);

    }

    public void GotoAPP()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
