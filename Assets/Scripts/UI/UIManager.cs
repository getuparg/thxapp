﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public UIPanel[] panels;
    public UIPanel initialPanel;
    
    private FooterPanel footer;
    private LoadVuMark vumarkManager;

    [Header("Videos")]
    public Transform videosParent;
    public GameObject videoPrefab;
    public float heightPervideo = 912f;
    public float initialPosY = -1024f;

    private void Awake()
    {
        footer = FindObjectOfType<FooterPanel>();
        vumarkManager = FindObjectOfType<LoadVuMark>();

        initialPanel.gameObject.SetActive(true);
        footer.initialButton.SelectButton();

    }

    public void QuitApp()
    {
        Application.Quit();
    }

    public void HidePanels()
    {
        foreach (UIPanel panel in panels)
        {
            panel.gameObject.SetActive(false);
        }
    }

    public void BackFromAR()
    {
        HidePanels();
        initialPanel.gameObject.SetActive(true);
        footer.DeselectButtons();
        footer.initialButton.SelectButton();
    }

    public IEnumerator LoadVideo(ExperienceObj video)
    {
       
        Debug.Log(video.thumbnailWide);
        GameObject go = Instantiate(videoPrefab, videosParent);
        UIVideo uiVideo = go.GetComponent<UIVideo>();

        if (video.thumbnailWide)
            uiVideo.thumbnailImage.overrideSprite = Sprite.Create(video.thumbnailWide, new Rect(0.0f, 0.0f, video.thumbnailWide.width, video.thumbnailWide.height), new Vector2(0.5f, 0.5f), 100.0f);

        uiVideo.videoPlayer.url = video.videoUrl;
        RectTransform rt = videosParent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, rt.sizeDelta.y + heightPervideo);
        
        videosParent.localPosition = new Vector2(0, initialPosY);
        yield return null;
    }
}
