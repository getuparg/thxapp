﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public string[] videos = new string[2];

    [Header("Video players")]
    public IntroVideo introVideoPlayer;
    public CloudVideo cloudVideoPlayer;


}
