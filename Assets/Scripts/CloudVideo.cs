﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CloudVideo : MonoBehaviour
{
    [Header("Cloud Video")]
    public VideoPlayer videoPlayer;
    public Material videoMaterial;
    public Animator animator;

    public void PlayAnimation()
    {
        animator.SetBool(AnimatorTags.FadeIn, true);
    }

    public void WaitUntilCloudAnimation()
    {
        videoPlayer.Play();
    }

}
