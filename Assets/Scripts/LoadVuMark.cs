﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Net;
using System.Globalization;
using System.Runtime.Serialization;

public class LoadVuMark : MonoBehaviour
{
    public string getUrl = "http://190.105.227.203/thxapi/getmarkers";
    public ExperiencesData experienceData;
    public List<ExperienceObj> markers;

    private UIManager uiManager;

    private void Awake()
    {
        uiManager = FindObjectOfType<UIManager>();
        StartCoroutine(GetRequest(getUrl));
    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                // Show results as text
                Debug.Log(webRequest.downloadHandler.text);

                experienceData = JsonUtility.FromJson<ExperiencesData>(webRequest.downloadHandler.text);
                DataTOObj();
            }
        }
    }

    private void DataTOObj()
    {
        StartCoroutine(DataTOObjRoutine());
    }

    IEnumerator DataTOObjRoutine()
    {
        foreach (Experience e in experienceData.data)
        {
            ExperienceObj obj = new ExperienceObj();
            obj.id = e.id;
            obj.markerID = e.markerID;
            obj.videoUrl = e.videoUrl;
            obj.secondVideoUrl = e.secondVideo;
            obj.isPLU = e.isPLU;

            if (e.uploadDate != "")
            {
                int year = Convert.ToInt32(e.uploadDate.Substring(0, 4));
                int month = Convert.ToInt32(e.uploadDate.Substring(5, 2));
                int day = Convert.ToInt32(e.uploadDate.Substring(8, 2));

                var time = new DateTime(year, month, day);
                obj.uploadDate = time;
            }

            yield return GetTexture(CreateSpriteFromTexture, e.thumbnail, obj, false);
            yield return GetTexture(CreateSpriteFromTexture, e.thumbnailwide, obj, true);

            yield return uiManager.LoadVideo(obj);

            markers.Add(obj);
        }
    }

    IEnumerator GetTexture(Action<Texture2D, ExperienceObj, bool> callback, string url, ExperienceObj obj, bool wide)
    {
        if (url != "")
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                callback(texture, obj, wide);
            }
        }
    }

    private void CreateSpriteFromTexture(Texture2D texture, ExperienceObj obj, bool wide = false)
    {
        if (texture == null)
            return;
        if (!wide)
        {
            obj.thumbnail = texture;
        }
        else
        {
            obj.thumbnailWide = texture;
        }

    }

    public ExperienceObj GetMarkerInfo(int id)
    {
        foreach (ExperienceObj e in markers)
        {
            if (e.markerID == id)
                return e;
        }
        return null;
    }

}


[Serializable]
public class ExperiencesData
{
    public Experience[] data;
}

[Serializable]
public class Experience
{
    public int id;
    public int markerID;
    public string description;
    public string videoUrl;
    public string markerUrl;
    public string secondVideo;
    public string thumbnail;
    public string thumbnailwide;
    public string uploadDate;
    public bool isPLU;
}
[Serializable]
public class ExperienceObj
{
    public int id;
    public int markerID;
    public string videoUrl;
    public string secondVideoUrl;
    public Texture2D thumbnail;
    public Texture2D thumbnailWide;
    public DateTime uploadDate;
    public bool isPLU;
}