﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class IntroVideo : MonoBehaviour
{
    [Header("Intro video")]
    public VideoPlayer videoPlayer;
    public Material videoMaterial;
    public Animator animator;

    [Header("Cloud video")]
    public CloudVideo cloudVideo;

    private void Update()
    {
        if (videoPlayer.isPlaying)
        {
            if (Mathf.Ceil((float)videoPlayer.time) == Mathf.Ceil((float)videoPlayer.length))
            {
                WaitUntilIntroVideoFinish();
            }
        }
    }

    public void PlayAnimation()
    {
        //animator.SetBool(AnimatorTags.FadeIn, true);
        videoPlayer.Play();
    }

    public void WaitUntilIntroAnimation()
    {
        videoPlayer.Play();
    }

    public void WaitUntilIntroVideoFinish()
    {
        cloudVideo.PlayAnimation();
    }
}
