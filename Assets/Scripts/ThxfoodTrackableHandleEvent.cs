﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using TMPro;
using UnityEngine.Video;
using UnityEngine.Networking;
using System;

public class ThxfoodTrackableHandleEvent : MonoBehaviour, ITrackableEventHandler
{
    #region PROTECTED_MEMBER_VARIABLES

    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    #endregion // PROTECTED_MEMBER_VARIABLES

    private VuMarkManager thxVuMarkManager;
    [SerializeField] private TextMeshProUGUI debugText;
    [SerializeField] private VideoManager videoManager;
    private ExperienceObj currentVideoExperience;

    public LoadVuMark vuMarksManager;
    #region UNITY_MONOBEHAVIOUR_METHODS

    protected virtual void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

        thxVuMarkManager = TrackerManager.Instance.GetStateManager().GetVuMarkManager();
    }


    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    #endregion // UNITY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName +
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS

    protected virtual void OnTrackingFound()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);
            var animatorComponents = mTrackableBehaviour.GetComponentsInChildren<Animator>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;

            foreach (var component in animatorComponents)
            {
                component.enabled = true;
            }
        }

        foreach (var item in thxVuMarkManager.GetActiveBehaviours())
        {
            int id = System.Convert.ToInt32(item.VuMarkTarget.InstanceId.NumericValue);
            currentVideoExperience = vuMarksManager.GetMarkerInfo(id);

            if (currentVideoExperience != null)
            {
                if (currentVideoExperience.videoUrl != "")
                {
                    videoManager.introVideoPlayer.videoPlayer.url = currentVideoExperience.videoUrl;
                    videoManager.introVideoPlayer.videoMaterial.SetTexture("_MainTex", currentVideoExperience.thumbnail);
                    videoManager.introVideoPlayer.videoMaterial.SetColor("_Color", Color.white);
                }

                if (currentVideoExperience.secondVideoUrl != "")
                {
                    videoManager.cloudVideoPlayer.videoPlayer.url = currentVideoExperience.secondVideoUrl;
                    videoManager.cloudVideoPlayer.videoMaterial.SetTexture("_MainTex", currentVideoExperience.thumbnailWide);
                    videoManager.cloudVideoPlayer.videoMaterial.SetColor("_Color", Color.white);
                }

                videoManager.introVideoPlayer.PlayAnimation();
            }
        }
    }


    protected virtual void OnTrackingLost()
    {
        //debugText.text = "No VuMark found.";


        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);
            var animatorComponents = mTrackableBehaviour.GetComponentsInChildren<Animator>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;

            foreach (var component in animatorComponents)
            {
                component.enabled = false;
            }

            videoManager.introVideoPlayer.videoPlayer.url = "";
            videoManager.cloudVideoPlayer.videoPlayer.url = "";

            videoManager.introVideoPlayer.videoMaterial.SetTexture("_MainTex", null);
            videoManager.cloudVideoPlayer.videoMaterial.SetTexture("_MainTex", null);

            videoManager.introVideoPlayer.animator.SetBool(AnimatorTags.FadeIn, false);
            videoManager.cloudVideoPlayer.animator.SetBool(AnimatorTags.FadeIn, false);

        }


    }

    #endregion // PROTECTED_METHODS

}
