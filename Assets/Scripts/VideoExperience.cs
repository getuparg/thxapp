using UnityEngine.Video;
using UnityEngine;

public class VideoExperience : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public GameObject videoQuad;
    public Material videoQuadMaterial;
    public Animator animator;

    private void Awake()
    {
        videoQuadMaterial = videoQuad.GetComponent<Renderer>().material;
    }

    public void PlayINAnimation()
    {
        animator.SetBool(AnimatorTags.FadeIn, true);
    }

    public void PlayOUTAnimation()
    {
        animator.SetBool(AnimatorTags.FadeIn, false);
    }
}

public class AnimatorTags
{
    public const string FadeIn = "FadeIn";
    public const string FadeOut = "FadeOut";
}