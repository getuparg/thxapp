﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FiltersPanel : MonoBehaviour
{
    public Image filtersContainer;
    private bool show = false;
    private float totalTimeToFillContainer = 0.5f;
    private float currentTime;

    private void OnEnable()
    {
        show = true;
        currentTime = 0f;
    }

    private void Update()
    {
        if (show)
        {
            if (currentTime < totalTimeToFillContainer)
            {
                float filled = currentTime / totalTimeToFillContainer;
                currentTime += Time.deltaTime;

                filtersContainer.fillAmount = filled;
            }
        }
    }

    private void OnDisable()
    {
        show = false;
    }

    public void ActiveFiltersContainer()
    {
        gameObject.SetActive(true);
    }
}
