﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public abstract class AbstractFilter : MonoBehaviour
{
    public TextMeshProUGUI filterDisplay;
    public string value;
    public Color activeColor;
    public Color inactiveColor;

    private Button button;
    private UIFilter uiFilter;
    private Container filtersContainer;

    private void Awake()
    {
        filtersContainer = FindObjectOfType<Container>();
        button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(OnClickFilterValue);
    }

    public void SetUIFilter(UIFilter uiFilter)
    {
        this.uiFilter = uiFilter;
    }

    public void OnClickFilterValue()
    {
        DeactivateBrothersFilters();
        uiFilter.SetValue(this.value);
        filterDisplay.color = activeColor;
        filtersContainer.ActiveChild(transform.parent.gameObject);
    }

    public void DisableFilter()
    {
        filterDisplay.color = inactiveColor;
    }

    private void DeactivateBrothersFilters()
    {
        Transform parent = transform.parent;
        AbstractFilter[] filters = parent.GetComponentsInChildren<AbstractFilter>();
        foreach (AbstractFilter af in filters)
        {
            af.DisableFilter();
        }
    }
    
}
