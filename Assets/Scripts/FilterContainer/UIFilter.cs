﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIFilter : MonoBehaviour
{
    public string currentValue;
    
    public GameObject activeFilterUI;
    public GameObject inactiveFilterUI;

    public TextMeshProUGUI currentValueDisplay;

    public void SetValue(string value)
    {
        currentValue = value;
        currentValueDisplay.text = currentValue;
        inactiveFilterUI.SetActive(false);
        activeFilterUI.SetActive(true);
    }

    public void CleanFilter()
    {
        currentValue = "";
        currentValueDisplay.text = currentValue;
        activeFilterUI.SetActive(false);
        inactiveFilterUI.SetActive(true);
    }
}
