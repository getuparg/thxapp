﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class FiltersController : MonoBehaviour
{
    public List<string> names;
    public List<string> countries;
    public List<string> months;
    public List<string> years;

    [Header("Names")]
    public UIFilter uiNameFilter;
    public Transform nameContainerPanel;
    public GameObject nameFilterPrefab;
    public Transform nameFilterParent;
    public float nameFilterObjectSize = 100f;

    [Header("Countries")]
    public UIFilter uiCountryFilter;
    public Transform countryContainerPanel;
    public GameObject countryFilterPrefab;
    public Transform countryFilterParent;
    public float countryFilterObjectSize = 100f;

    [Header("Months")]
    public UIFilter uiMonthFilter;
    public Transform monthContainerPanel;
    public GameObject monthFilterPrefab;
    public Transform monthFilterParent;
    public float monthFilterObjectSize = 100f;

    [Header("Years")]
    public UIFilter uiYearFilter;
    public Transform yearContainerPanel;
    public GameObject yearFilterPrefab;
    public Transform yearFilterParent;
    public float yearFilterObjectSize = 100f;



    private void Awake()
    {
        names = new List<string>(){ "Isas", "Emir", "Marcela", "Fernando", "Juan", "Leo", "Matias", "Fede" };
        countries = new List<string>(){ "Argentina", "Usa", "Ecuador", "Brasil", "Mexico", "Paraguay", "España", "Ucrania" };
        months = new List<string>(){ "January", "Febrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        years = new List<string>(){ "2019","2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"};
        LoadFiltersValues();
    }

    private void Start()
    {
        
    }

    private void LoadFiltersValues()
    {
        foreach (string name in names)
        {
            GameObject go = Instantiate(nameFilterPrefab, nameFilterParent);
            NameFilter nameFilter = go.GetComponent<NameFilter>();
            nameFilter.SetUIFilter(uiNameFilter);
            nameFilter.value = name;
            nameFilter.filterDisplay.text = name;
            nameContainerPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(nameContainerPanel.GetComponent<RectTransform>().sizeDelta.x, nameContainerPanel.GetComponent<RectTransform>().sizeDelta.y + nameFilterObjectSize);
        }

        foreach (string country in countries)
        {
            GameObject go = Instantiate(countryFilterPrefab, countryFilterParent);
            CountryFilter countryFilter = go.GetComponent <CountryFilter>();
            countryFilter.SetUIFilter(uiCountryFilter);
            countryFilter.value = country;
            countryFilter.filterDisplay.text = country;
            countryContainerPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(countryContainerPanel.GetComponent<RectTransform>().sizeDelta.x, countryContainerPanel.GetComponent<RectTransform>().sizeDelta.y + countryFilterObjectSize);
        }

        foreach (string month in months)
        {
            GameObject go = Instantiate(monthFilterPrefab, monthFilterParent);
            MonthFilter monthFilter = go.GetComponent<MonthFilter>();
            monthFilter.SetUIFilter(uiMonthFilter);
            monthFilter.value = month;
            monthFilter.filterDisplay.text = month;
            monthContainerPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(monthContainerPanel.GetComponent<RectTransform>().sizeDelta.x, monthContainerPanel.GetComponent<RectTransform>().sizeDelta.y + monthFilterObjectSize);
        }

        foreach (string year in years)
        {
            GameObject go = Instantiate(yearFilterPrefab, yearFilterParent);
            YearFilter yearFilter = go.GetComponent<YearFilter>();
            yearFilter.SetUIFilter(uiYearFilter);
            yearFilter.value = year;
            yearFilter.filterDisplay.text = year;
            yearContainerPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(yearContainerPanel.GetComponent<RectTransform>().sizeDelta.x, yearContainerPanel.GetComponent<RectTransform>().sizeDelta.y + yearFilterObjectSize);
        }
    }
}
