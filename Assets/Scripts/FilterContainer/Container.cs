﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Container : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] private float maxSize;
    [SerializeField] private int minElements;

    private ContentSizeFitter contentSizeFilter;
    public List<GameObject> optionsLists;
    public List<UIFilter> uiFilters;

    public GameObject applyFiltersButton;
    public GameObject clearFiltersButton;

    public GameObject[] whiteSpaces;

    public Transform topPointHolder;
    public Transform bottomPointHolder;

    public Transform topMenu;
    public Transform bottomMenu;

    private void Awake()
    {
        contentSizeFilter = GetComponent<ContentSizeFitter>();
    }

    private void Update()
    {
        SetTopBottomMenu();

        if (GetActiveChilds() <= minElements)
        {
            contentSizeFilter.verticalFit = ContentSizeFitter.FitMode.MinSize;
        }
        else
        {
            contentSizeFilter.verticalFit = ContentSizeFitter.FitMode.Unconstrained;
            contentSizeFilter.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(contentSizeFilter.transform.GetComponent<RectTransform>().sizeDelta.x, maxSize);
        }

        if (NeedToActiveFilterButton())
        {
            applyFiltersButton.SetActive(true);
            clearFiltersButton.SetActive(true);
            foreach (GameObject g in whiteSpaces)
            {
                g.SetActive(true);
            }
        }
        else
        {
            applyFiltersButton.SetActive(false);
            clearFiltersButton.SetActive(false);
            foreach (GameObject g in whiteSpaces)
            {
                g.SetActive(false);
            }
        }
    }

    public void ActiveChild(GameObject g)
    {
        foreach (GameObject go in optionsLists)
        {
            if (go != g)
                go.SetActive(false);
        }
        g.SetActive(!g.activeInHierarchy);
    }

    private int GetActiveChilds()
    {
        int count = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeInHierarchy)
            {
                count++;
            }
        }
        return count;
    }

    public void ClearFilters()
    {
        foreach (GameObject g in optionsLists)
        {
            foreach (AbstractFilter af in g.transform.GetComponentsInChildren<AbstractFilter>())
            {
                af.DisableFilter();
            }
        }

        foreach (UIFilter uiFilter in uiFilters)
        {
            uiFilter.CleanFilter();
        }
    }

    private bool NeedToActiveFilterButton()
    {
        foreach (GameObject go in optionsLists)
        {
            if (go.activeInHierarchy)
                return true;
        }

        return false;
    }

    private void SetTopBottomMenu()
    {
        topMenu.position = topPointHolder.transform.position - new Vector3(0, 3, 0);
        bottomMenu.position = bottomPointHolder.transform.position + new Vector3(0,2,0);
    }
}
