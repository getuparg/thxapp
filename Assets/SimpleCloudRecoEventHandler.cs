﻿using UnityEngine;
using UnityEngine.Video;
using Vuforia;

public class SimpleCloudRecoEventHandler : MonoBehaviour, IObjectRecoEventHandler
{
    public ImageTargetBehaviour ImageTargetTemplate;
    private CloudRecoBehaviour mCloudRecoBehaviour;
    private bool mIsScanning = false;
    private string mTargetMetadata = "";

    // Register cloud reco callbacks
    void Awake()
    {
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        mCloudRecoBehaviour.RegisterEventHandler(this);
    }
    //Unregister cloud reco callbacks when the handler is destroyed
    void OnDestroy()
    {
        mCloudRecoBehaviour.UnregisterEventHandler(this);
    }

    public void OnInitialized(TargetFinder targetFinder)
    {
        Debug.Log("Cloud Reco initialized");
    }
    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log("Cloud Reco init error " + initError.ToString());
    }
    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log("Cloud Reco update error " + updateError.ToString());
    }
    public void OnStateChanged(bool scanning)
    {
        mIsScanning = scanning;
        if (scanning)
        {
            // clear all known trackables
            var tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            tracker.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);
        }
    }

    // Here we handle a cloud target recognition event
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        //TargetFinder.CloudRecoSearchResult cloudRecoSearchResult =
        //    (TargetFinder.CloudRecoSearchResult)targetSearchResult;

        // do something with the target metadata
        //mTargetMetadata = cloudRecoSearchResult.MetaData;

        // stop the target finder (i.e. stop scanning the cloud)
        // mCloudRecoBehaviour.CloudRecoEnabled = false;

        var t = TrackerManager.Instance.GetTracker<ObjectTracker>();
        t.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);

        // Build augmentation based on target 
        if (ImageTargetTemplate)
        {
            Debug.Log("--------------- Cloud Target id: " + targetSearchResult.UniqueTargetId + " ---------------");
            // enable the new result with the same ImageTargetBehaviour: 
            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            tracker.GetTargetFinder<ImageTargetFinder>().EnableTracking(targetSearchResult, ImageTargetTemplate.gameObject);

            ImageTargetTemplate.transform.GetChild(0).gameObject.SetActive(true);
            VideoPlayer videoPlayer = ImageTargetTemplate.transform.GetChild(0).GetComponent<VideoPlayer>();
            if(!videoPlayer.isPlaying)
                videoPlayer.Play();
        }
    }

    void OnGUI()
    {
        // Display current 'scanning' status
        GUI.Box(new Rect(100, 100, 200, 50), mIsScanning ? "Scanning" : "Not scanning");
        // Display metadata of latest detected cloud-target
        GUI.Box(new Rect(100, 200, 200, 50), "Metadata: " + mTargetMetadata);
        // If not scanning, show button
        // so that user can restart cloud scanning
        if (!mIsScanning)
        {
            if (GUI.Button(new Rect(100, 300, 200, 50), "Restart Scanning"))
            {
                // Restart TargetFinder
                mCloudRecoBehaviour.CloudRecoEnabled = true;
            }
        }
    }
}